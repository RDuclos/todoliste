package com.example.todolist;

import android.content.Context;
import android.media.Image;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.google.android.material.textfield.TextInputEditText;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ListeAdapter extends ArrayAdapter<Taches> {
        Context context;
        public ListeAdapter(Context context, List<Taches> listeTache){
            super(context, -1, listeTache);
            this.context = context;
        }
        @RequiresApi(api = Build.VERSION_CODES.O)
        public View getView(int position, View convertView, ViewGroup parent){
            View view;
            Taches uneTache;
            view = null;
            if(convertView == null){
                LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = layoutInflater.inflate(R.layout.activity_ligne_tache, parent, false );
            }
            else
            {
                view = convertView;
            }
            uneTache = getItem(position);
            TextView TvTitre = (TextView)view.findViewById(R.id.tvTitre);
            TextView TvDescription = (TextView)view.findViewById(R.id.tvTexte);
            TextView TvDatelimite = (TextView)view.findViewById(R.id.tvDateLimite);
            ImageView imageDegres = (ImageView)view.findViewById(R.id.ivDegres);
            if(uneTache.getDegres() == 4){
                imageDegres.setImageDrawable(context.getDrawable(R.drawable.violet));
            }
            else{
                if(uneTache.getDegres()==3){
                    imageDegres.setImageDrawable(context.getDrawable(R.drawable.rouge));
                }
                else{
                    if(uneTache.getDegres()==2){
                        imageDegres.setImageDrawable(context.getDrawable(R.drawable.orange));
                    }
                    else {
                        imageDegres.setImageDrawable(context.getDrawable(R.drawable.vert));
                    }
                }
            }
            TvTitre.setText(uneTache.getTitre());

            String description = uneTache.getDescription();

            /*StringBuilder sb = new StringBuilder(description);
            StringBuilder after;
            for(int i = 0; i < (sb.length()); i++){

                if(i>10){
                    after = new StringBuilder(sb.deleteCharAt(i).toString());
                    sb = after;
                    Toast.makeText(context.getApplicationContext(),""+ i,Toast.LENGTH_SHORT).show();
                }
            }
            if(after == null)
            {
                String resultString = "chier";
            }
            else{
                String resultString = after.toString();
            }

            */
            TvDescription.setText(description);
            //LocalDate DateCreation = LocalDate.parse(uneTache.getDatelimite());
            String [] dateParts = uneTache.getDatelimite().split("-");
            String annee = dateParts[0];
            String mois = dateParts[1];
            String jour = dateParts[2];
            LocalDate DateLimite = LocalDate.parse(uneTache.getDatelimite());
            LocalDate aujourdui = LocalDate.now();
            long daysBetween = Duration.between(aujourdui.atStartOfDay(), DateLimite.atStartOfDay()).toDays();
            String message ="";
            if(daysBetween==0){
                message = jour + " / " + mois + " / " + annee + "  Aujourd'hui";
            }
            else{
                if(daysBetween==1){
                     message = jour + " / " + mois + " / " + annee + "  Demain";
                }
                else{
                    if(daysBetween==2){
                         message = jour + " / " + mois + " / " + annee + "  Après-Demain";
                    }
                    else{
                         message = jour + " / " + mois + " / " + annee + "  Dans " + daysBetween + " jours";
                    }


                }
            }


            TvDatelimite.setText(message);
        return view;
        }


}
