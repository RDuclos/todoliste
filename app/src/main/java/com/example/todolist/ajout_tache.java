package com.example.todolist;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.example.todolist.MainActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ajout_tache extends AppCompatActivity {
    Button btvalider;
    EditText nom;
    EditText description;
    EditText degres;
    CalendarView calendrier;
    String DateFinal;
    private int choixdegres;
    private Spinner spDegres;
    private DatabaseManager dbm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ajout_tache);
        spDegres = (Spinner) findViewById(R.id.spdegres);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.degres,android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spDegres.setAdapter(adapter);
        spDegres.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String spinnertxt = adapterView.getItemAtPosition(i).toString();
                if(spinnertxt.equals("Faible"))
                {
                    choixdegres=1;
                }
                else{
                    if(spinnertxt.equals("Moyen")){
                        choixdegres=2;

                    }
                    else{
                        if(spinnertxt.equals("Elevé")){
                            choixdegres=3;
                        }
                        else{
                            choixdegres=4;
                        }
                    }
                }
                //Toast.makeText(adapterView.getContext(),choixniveau,Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        dbm = new DatabaseManager(this);
        btvalider = (Button) findViewById(R.id.buttonvalidertache);
        nom = (EditText) findViewById(R.id.tbnom);
        description = (EditText) findViewById(R.id.tbdescription);

        calendrier = (CalendarView) findViewById(R.id.calendarView);
        calendrier.setOnDateChangeListener((new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                month++;
                String mois = "";
                String jour = "";
                if(month<10){
                    mois = "0"+month;
                }
                else{
                    mois = mois+month;
                }
                if(dayOfMonth<10){
                    jour = "0" + dayOfMonth;
                }
                else {
                    jour = jour + dayOfMonth;
                }

                DateFinal = year + "-" + mois + "-" + jour;
            }
        }));
        btvalider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(DateFinal == null){
                    Toast.makeText(getApplicationContext(),"Merci de selectionner une date !",Toast.LENGTH_SHORT).show();
                }
                else{
                    if(!nom.getText().toString().equals(""))
                    {

                            toajouttache();


                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(),"Merci de selectionner un titre !",Toast.LENGTH_SHORT).show();
                    }

                }


            }
        });
    }

    public void toajouttache(){


        if(dbm.ajoutTache(nom.getText().toString(), description.getText().toString(), DateFinal, choixdegres) == false){
            Toast.makeText(getApplicationContext(),"Une erreur est survenue lors de la création de la tâche",Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"La tâche à été ajouté",Toast.LENGTH_SHORT).show();
            finish();
        }







    }


}