package com.example.todolist;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.time.Duration;
import java.time.LocalDate;

public class Tache_Details extends AppCompatActivity {
    private int id;
    private DatabaseManager dbm;
    TextView tvTitre;
    TextView tvDescription;
    TextView tvUrgence;
    TextView tvNbJours;
    Button btSupp;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tache__details);
        Intent intent = getIntent();
        id = intent.getIntExtra("id", 0);

        dbm = new DatabaseManager(this);
        dbm = new DatabaseManager(this);
        dbm.actualiserdegres();
        Taches LaTache = dbm.lectureTache(id);

        btSupp = findViewById(R.id.btsupp);
        tvTitre = findViewById(R.id.tvTitre_Details);
        tvDescription = findViewById(R.id.tvdescription);
        tvUrgence = findViewById(R.id.tvurgence);
        tvNbJours = findViewById(R.id.tvnbjours);

        tvTitre.setText(LaTache.getTitre());
        tvDescription.setText(LaTache.getDescription());
        int degres = LaTache.getDegres();
        if(degres == 1){
            tvUrgence.setText("Tache de faible importance.");
            tvUrgence.setTextColor(Color.GREEN);
        }
        else
        {
            if(degres == 2){
                tvUrgence.setText("Tache d'importance moyenne.");
                tvUrgence.setTextColor(getResources().getColor(R.color.orange));
            }
            else{
                if(degres == 3)
                {
                    tvUrgence.setText("Tache importante.");
                    tvUrgence.setTextColor(Color.RED);
                }
                else{
                    tvUrgence.setText("Tache urgente !");
                    tvUrgence.setTextColor(Color.MAGENTA);
                }
            }
        }
        String datelimite = LaTache.getDatelimite();
        LocalDate DateLimite = LocalDate.parse(datelimite);
        LocalDate aujourdui = LocalDate.now();
        long daysBetween = Duration.between(aujourdui.atStartOfDay(), DateLimite.atStartOfDay()).toDays();
        String message="";
        if(daysBetween == 0){
            message = "La tâche est à finir pour aujourd'hui";
        }
        else
        {
            if(daysBetween==1){
                message = "La tâche est à finir dans 1 jour.";
            }
            else
            {
                message = "La tâche est à finir dans " + daysBetween + " jours.";
            }
        }

        tvNbJours.setText(message);
        btSupp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tosupptache();



            }
        });

    }
    public void tosupptache(){

        if(dbm.suppTache(id) == false){
            Toast.makeText(getApplicationContext(),"Une erreur est survenue lors de la suppression de la tâche",Toast.LENGTH_SHORT).show();
        }
        else{
            Toast.makeText(getApplicationContext(),"La tâche à été complétée",Toast.LENGTH_SHORT).show();
            finish();
        }

    }
}