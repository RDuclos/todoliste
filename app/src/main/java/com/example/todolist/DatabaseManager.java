package com.example.todolist;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;

public class DatabaseManager extends SQLiteOpenHelper {
    private static final String DATABASE_NAME="todolist.db";
    private static final int DATABASE_VERSION=16;

    public DatabaseManager (Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    int id;
    String titre;
    String description;
    String datelimite;
    int degres;
    SQLiteDatabase base;
    @Override
    public void onCreate(SQLiteDatabase db) {
        this.base = db;
        String strSqlE = "create table taches(id  Integer   primary key autoincrement not null,  titre String , description String , datelimite String,degres int )";
        db.execSQL(strSqlE);




        Log.i("DATABASE", "onCreate invoqué");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String strSqlDelete = "drop table Taches";
        db.execSQL(strSqlDelete);
        this.onCreate(db);
    }
    public ArrayList<Taches> lectureTaches(){
        ArrayList<Taches> taches = new ArrayList<>();
        String sqlE = "select id, titre, description, datelimite, degres, DATE('now') from taches  where datelimite >= DATE('now')  order by degres DESC, datelimite ASC";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlE, null);
        curseur.moveToFirst();


        while(!curseur.isAfterLast()){
            Taches tache = new Taches(curseur.getInt(0), curseur.getString(1), curseur.getString(2), curseur.getString(3), curseur.getInt(4));
            taches.add(tache);
            curseur.moveToNext();

        }
        curseur.close();
        return(taches);
    }
    public boolean ajoutTache(String ptitre, String pdescription, String pdatelimite, int pdegres){
        boolean succes = true;
        SQLiteDatabase base = getWritableDatabase();

        pdescription = pdescription.replace("'", "''");
        ptitre = ptitre.replace("'", "''");
        String req= "insert into taches (titre ,  description, datelimite, degres) values ( '" + ptitre + "' , '"+pdescription+"', '"+pdatelimite+"', "+pdegres+")";

        try {
            base.execSQL(req);
        }
        catch(Exception e) {
            succes = false;
            //  Block of code to handle errors
        }

        //Connection connexion = DriverManager.getConnection("sqlite:");
        //PreparedStatement sql=  connexion.prepareStatement("insert into taches (titre ,  description, datelimite, degres) values (?, ?, ?, ?)");
        //sql.setString(1, ptitre);
        //sql.setString(2, pdescription);
        //sql.setString(3, pdatelimite);
        //sql.setInt(4, degres);
        //sql.executeUpdate();




        return succes;
    }
    public boolean suppTache(int id) {
        boolean succes = true;
        SQLiteDatabase base = getWritableDatabase();


        String req= "DELETE FROM `taches` where id = " + id;

        try {
            base.execSQL(req);
        }
        catch(Exception e) {
            succes = false;
            //  Block of code to handle errors
        }

        return succes;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void actualiserdegres(){
        LocalDate aujourdui = LocalDate.now();
        SQLiteDatabase base = getWritableDatabase();

        String sqlE = "select id, titre, description, datelimite, degres from taches  where datelimite >= date()  order by degres DESC";
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlE, null);
        curseur.moveToFirst();
        while(!curseur.isAfterLast()){
            String date = curseur.getString(3);
            int degres = curseur.getInt(4);
            LocalDate DateLimite = LocalDate.parse(date);
            int idT = curseur.getInt(0);
            long daysBetween = Duration.between(aujourdui.atStartOfDay(), DateLimite.atStartOfDay()).toDays();
            if((daysBetween < 3) & (degres<=3))
            {
                    sqlE = "UPDATE taches SET degres = '4' WHERE id = "+ idT +"";
                    SQLiteDatabase basee = getWritableDatabase();
                    basee.execSQL(sqlE);

            }
            else{
                if((daysBetween < 5) & (degres<=2))
                {
                    sqlE = "UPDATE taches SET degres = '3' WHERE id = "+ idT +"";
                    SQLiteDatabase basee = getWritableDatabase();
                    basee.execSQL(sqlE);
                }
                else{
                    if((daysBetween < 8) & (degres<=1))
                    {
                        sqlE = "UPDATE taches SET degres = '2' WHERE id = "+ idT +"";
                        SQLiteDatabase basee = getWritableDatabase();
                        basee.execSQL(sqlE);
                    }

                }

            }




            curseur.moveToNext();
        }
        curseur.close();
    }

    public Taches lectureTache(int idEquipe){


        String sqlM = "select id, titre, description, datelimite, degres from taches where id = " + idEquipe;
        Cursor curseur = this.getReadableDatabase().rawQuery(sqlM, null);
        curseur.moveToFirst();

        Taches tache = new Taches(curseur.getInt(0), curseur.getString(1), curseur.getString(2), curseur.getString(3), curseur.getInt(4));

        curseur.close();
        return(tache);
    }
}
