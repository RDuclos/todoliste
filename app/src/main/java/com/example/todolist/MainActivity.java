package com.example.todolist;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity {
    ListView lvListe;
    ArrayList<Taches> listeTaches;

    private DatabaseManager dbm;
    Button boutonajout;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbm = new DatabaseManager(this);
        dbm = new DatabaseManager(this);
        dbm.actualiserdegres();

        boutonajout = (Button)findViewById(R.id.btajout);


        listeTaches = dbm.lectureTaches();
        //listeTaches = new DatabaseManager(this).lectureTaches();
        lvListe = (ListView) findViewById(R.id.listviewtache);

        lvListe.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startViewActivity(position);
            }
        });
        boutonajout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tonvxtache();

            }
        });

    }
    @Override
    protected void onResume() {
        super.onResume();
        ListeAdapter listeAdapter = new ListeAdapter(this,listeTaches);
        lvListe.setAdapter(listeAdapter);
        listeAdapter.notifyDataSetChanged();
    }
    private void startViewActivity(int i){
        Taches uneTache = listeTaches.get(i);
        Intent intent = new Intent(this, Tache_Details.class);
        intent.putExtra("id", uneTache.getId() );
        startActivity(intent);
    }


    @Override
    protected void onRestart() {
        super.onRestart();
        this.recreate();
    }
    public void tonvxtache()
    {
        Intent intent = new Intent(this,ajout_tache.class);
        startActivity(intent);
    }
    public void actualiser(){
        this.recreate();
    }
}