package com.example.todolist;

public class Taches {
    int id;
    String titre;
    String description;
    String datelimite;
    int degres;

    public int getDegres() {
        return degres;
    }

    public void setDegres(int degres) {
        this.degres = degres;
    }


    public Taches(int pid, String ptitre, String pdescription, String pdatelimite, int pdegres) {
        this.id = pid;
        this.description=pdescription;
        this.titre = ptitre;
        this.datelimite = pdatelimite;
        this.degres = pdegres;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDatelimite() {
        return datelimite;
    }

    public void setDatelimite(String datelimite) {
        this.datelimite = datelimite;
    }

}
